#define _POSIX_C_SOURCE 200112L
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>

#define READ_BUFF_SIZE 4096
#define SERVER_HOST "telehack.com"
#define SERVER_PORT "telnet"
// Завершения программы с закрытием сокета
void terminateConnection(int sockDesc) {
    if (sockDesc >= 0) {
        shutdown(sockDesc, SHUT_RDWR);
        close(sockDesc);
    }
    exit(EXIT_FAILURE);
}

// Установки соединения с сервером
int setupSocketAndConnect() {
    struct addrinfo connectionHints = {0};
    connectionHints.ai_family = AF_INET;
    connectionHints.ai_socktype = SOCK_STREAM;
    connectionHints.ai_protocol = IPPROTO_TCP;

    struct addrinfo *serverInfo;
    // Получение информации для подключения к серверу
    int status = getaddrinfo(SERVER_HOST, SERVER_PORT, &connectionHints, &serverInfo);
    if (status != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status));
        return -1;
    }

    int socketDescriptor = -1;
    // Обход результатов попытка подключиться
    for (struct addrinfo *res = serverInfo; res != NULL; res = res->ai_next) {
        // Создание сокета с указанными атрибутами
        socketDescriptor = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
        if (socketDescriptor < 0) continue;
        if (connect(socketDescriptor, res->ai_addr, res->ai_addrlen) == 0) break;
        close(socketDescriptor);
        socketDescriptor = -1;
    }

    freeaddrinfo(serverInfo);

    return socketDescriptor;
}
// Приём данных из сокета
int receiveFromSocket(int sockDesc, char* dataBuffer) {
    memset(dataBuffer, 0, READ_BUFF_SIZE);

    int totalReceived = 0, bytesReceived = 0;
    // Получение данных из сокета
    while ((bytesReceived = recv(sockDesc, &dataBuffer[totalReceived], READ_BUFF_SIZE - totalReceived - 1, 0)) > 0) {
        totalReceived += bytesReceived;
        // Проверка на конец передачи данных
        if (totalReceived >= 2 && dataBuffer[totalReceived - 1] == '.' && dataBuffer[totalReceived - 2] == '\n') break;
    }
    return bytesReceived <= 0 ? -1 : 0;
}

int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <font> <text>\n", argv[0]);
        return EXIT_FAILURE;
    }
    // Установка соединения с сервером
    int socketDescriptor = setupSocketAndConnect();
    if (socketDescriptor < 0) {
        fprintf(stderr, "Connection failed\n");
        return EXIT_FAILURE;
    }
    // Буферы для хранения входных и выходных данных    
    char receivedDataBuffer[READ_BUFF_SIZE] = {0};
    char fontCommand[1024] = {0};
    snprintf(fontCommand, sizeof(fontCommand), "figlet /%s %s\r\n", argv[1], argv[2]);
    // Проверка на получение данных с сервера перед отправкой команды
    if (receiveFromSocket(socketDescriptor, receivedDataBuffer) < 0) {
        fprintf(stderr, "Receive data error.\n");
        terminateConnection(socketDescriptor);
    }
    // Отправка команды серверу
    if (send(socketDescriptor, fontCommand, strlen(fontCommand), 0) < 0) {
        fprintf(stderr, "Send error.\n");
        terminateConnection(socketDescriptor);
    }
    // Повторный прием данных с сервера, содержащих ответ на команду
    if (receiveFromSocket(socketDescriptor, receivedDataBuffer) < 0) {
        fprintf(stderr, "Receive data error.\n");
        terminateConnection(socketDescriptor);
    }
    // Вывод данных на экран
    puts(receivedDataBuffer);
    terminateConnection(socketDescriptor);

}

